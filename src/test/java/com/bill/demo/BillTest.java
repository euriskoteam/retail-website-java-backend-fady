package com.bill.demo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bill.demo.domain.bill.Bill;
import com.bill.demo.domain.customer.Affiliate;
import com.bill.demo.domain.customer.Customer;
import com.bill.demo.domain.customer.Employee;
import com.bill.demo.domain.item.Item;
import com.bill.demo.domain.item.ItemType;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class BillTest extends TestCase {

	List<Item> items;

	Employee employee;

	Affiliate affiliate;

	Customer oldCustomer, newCustomer;

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public BillTest(String testName) {
		super(testName);

		final double price = 100;

		items = new ArrayList<>();
		items.add(new Item(ItemType.GROCERY, price));
		items.add(new Item(ItemType.GROCERY, price));
		items.add(new Item(ItemType.GROCERY, price));
		items.add(new Item(ItemType.NON_GROCERY, price));
		items.add(new Item(ItemType.NON_GROCERY, price));

		employee = new Employee();

		affiliate = new Affiliate();

		Calendar olderThan2Years = Calendar.getInstance();
		olderThan2Years.add(Calendar.YEAR, -2);

		oldCustomer = new Customer(olderThan2Years.getTime());
		newCustomer = new Customer(new Date());

	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(BillTest.class);
	}

	/**
	 * Bill Test :-)
	 */
	public void testBill() {

		Bill employeeBill = new Bill(employee, items);

		assertEquals("testing bill customer", employee, employeeBill.getCustomer());
		assertEquals("testing bill items", items, employeeBill.getItems());

		// Employee To Pay Test
		// employee discount 0.3 => price to pay * (1 - 0.3)
		// 2 Non Grocery 100 USD per item => 200 * (1 - 0.3)
		// 3 Grocery 100 USD per item => 300
		// For every 100 USD give back 5 USD => 5% => To Pay 95%
		//
		double employeeExpectedAmount = 0.95 * (3 * 100 + 2 * 100 * (1 - Employee.RATE));
		assertEquals("testing employee bill amount", employeeExpectedAmount, employeeBill.amountToPay());

		// Affiliate To Pay Test
		//
		Bill affiliateBill = new Bill(affiliate, items);
		double affiliateExpectedAmount = 0.95 * (3 * 100 + 2 * 100 * (1 - Affiliate.RATE));
		assertEquals("testing affiliate bill amount", affiliateExpectedAmount, affiliateBill.amountToPay());

		// Old Customer To Pay Test
		//
		Bill oldCustomerBill = new Bill(oldCustomer, items);
		double oldCustExpectedAmount = 0.95 * (3 * 100 + 2 * 100 * (1 - Customer.TWO_YEARS_OLD_RATE));
		assertEquals("testing old customer bill amount", oldCustExpectedAmount, oldCustomerBill.amountToPay());

		// New Customer To Pay Test
		//
		Bill newCustomerBill = new Bill(newCustomer, items);
		double newCustExpectedAmount = 0.95 * (3 * 100 + 2 * 100 * (1 - Customer.FRESH_RATE));
		assertEquals("testing new customer bill amount", newCustExpectedAmount, newCustomerBill.amountToPay());

	}

}
