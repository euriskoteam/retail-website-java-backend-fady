package com.bill.demo;

import java.util.Calendar;
import java.util.Date;

import com.bill.demo.domain.customer.Affiliate;
import com.bill.demo.domain.customer.BaseCustomer;
import com.bill.demo.domain.customer.Customer;
import com.bill.demo.domain.customer.Employee;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Customer Unit testing.
 */
public class CustomerTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public CustomerTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of Customers tests
	 */
	public static Test suite() {
		return new TestSuite(CustomerTest.class);
	}

	/**
	 * Employee Test
	 */
	public void testEmployee() {

		double expectedRate = 0.3;

		BaseCustomer customer = new Employee();

		assertTrue("testing employee customer", customer.discountPercentage() == expectedRate);
	}

	/**
	 * Affiliate Test
	 */
	public void testAffiliate() {

		double expectedRate = 0.1;

		BaseCustomer customer = new Affiliate();

		assertEquals("testing affiliate customer", expectedRate, customer.discountPercentage());
	}

	/**
	 * Normal Customer Test
	 */
	public void testCustomer() {

		double expectedRate = 0.05;

		Calendar olderThan2Years = Calendar.getInstance();
		olderThan2Years.add(Calendar.YEAR, -2);

		BaseCustomer oldCustomer = new Customer(olderThan2Years.getTime());
		assertEquals("testing 2 years old customer discount", expectedRate, oldCustomer.discountPercentage());

		expectedRate = 0;
		BaseCustomer newCustomer = new Customer(new Date());
		assertEquals("testing new customer discount", expectedRate, newCustomer.discountPercentage());
	}

}
