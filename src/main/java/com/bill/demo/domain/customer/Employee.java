package com.bill.demo.domain.customer;

public class Employee extends BaseCustomer {

	public static final double RATE = 0.3;

	@Override
	public double discountPercentage() {

		return RATE;
	}

}
