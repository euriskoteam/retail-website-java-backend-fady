package com.bill.demo.domain.customer;

import java.util.Calendar;
import java.util.Date;

public class Customer extends BaseCustomer {

	public static final double TWO_YEARS_OLD_RATE = 0.05;

	public static final double FRESH_RATE = 0;

	Date registered;

	public Customer(Date registered) {
		this.registered = registered;
	}

	@Override
	public double discountPercentage() {

		Calendar since2years = Calendar.getInstance();

		/*
		 * get back 2 years with today
		 */
		since2years.add(Calendar.YEAR, -2);
		since2years.add(Calendar.DATE, 1);

		if (registered.before(since2years.getTime())) {
			return TWO_YEARS_OLD_RATE;
		} else {
			return FRESH_RATE;
		}
	}

}
