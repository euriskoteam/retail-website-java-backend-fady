package com.bill.demo.domain.customer;

public class Affiliate extends BaseCustomer {

	public static final double RATE = 0.1;

	@Override
	public double discountPercentage() {

		return RATE;
	}

}
