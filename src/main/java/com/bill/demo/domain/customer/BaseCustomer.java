package com.bill.demo.domain.customer;

public abstract class BaseCustomer {

	/**
	 * calculate the customer discount
	 * 
	 * @return discount
	 */
	public abstract double discountPercentage();
}