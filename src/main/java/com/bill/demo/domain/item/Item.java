package com.bill.demo.domain.item;

public class Item {

	private ItemType type;

	private double price;

	public Item(ItemType type, double price) {
		super();
		this.price = price;
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ItemType getType() {
		return type;
	}

	public void setType(ItemType type) {
		this.type = type;
	}

}
