package com.bill.demo.domain.item;

public enum ItemType {

	GROCERY, NON_GROCERY

}
